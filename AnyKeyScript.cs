using UnityEngine;

public class AnyKeyScript : MonoBehaviour
{
    public bool IsActive;
    public float timer;
    public GameObject AnyKeyText, Transition;
    private Animator TransitionAnimator;

    void Start()
    {
        Cursor.visible = false;
        TransitionAnimator = Transition.GetComponent<Animator>();
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (IsActive)
            {
                AnyKeyText.SetActive(true);
                timer = 1f;
                IsActive = false;
            }
            else
            {
                AnyKeyText.SetActive(false);
                timer = 0.25f;
                IsActive = true;
            }
        }
        if (Input.anyKey)
        {
            TransitionAnimator.Play("RightToLeft");
        }
    }
}
