using UnityEngine;
using UnityEngine.InputSystem;

public class ShipScript : MonoBehaviour
{
    public AudioClip ShootSound, Explosion;
    public GameObject ProjectilePrefab, UIManager, Canvas;
    public float Speed, ShootTimer, ShootCooldown;
    public bool alive;
    private AudioSource AudioSource;
    private Vector2 InputVector;
    private Rigidbody2D ShipRigibody;
    private Animator ShipAnimator;
    private UIManagerScript UIScript;
    private SceneManagerScript Pause;

    void Start()
    {
        Cursor.visible = false;
        alive = true;
        ShipRigibody = GetComponent<Rigidbody2D>();
        ShipAnimator = GetComponent<Animator>();
        UIScript = UIManager.GetComponent<UIManagerScript>();
        Pause = Canvas.GetComponent<SceneManagerScript>();
        AudioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        if (alive == true)
        {
            ShipRigibody.velocity = InputVector * Speed;
            if (ShootTimer > 0)
            {
                ShootTimer -= Time.deltaTime * ShootCooldown;
            }
        }
    }
    public void OnMove(InputAction.CallbackContext context)
    {
        InputVector = context.ReadValue<Vector2>();
    }

    public void OnShot(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            if (alive == true)
            {
                if (Pause.IsPaused == false)
                {
                    if (ShootTimer <= 0)
                    {
                        Instantiate(ProjectilePrefab, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
                        ShootTimer++;
                        AudioSource.PlayOneShot(ShootSound, 0.5f);
                    }
                }
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ProjectileAlien")
        {
            UIScript.LivesMinusOne();
            Destroy(collision.gameObject);
            PlayerDeath();
            AudioSource.PlayOneShot(Explosion, 0.5f);
        }
    }

    public void AliveToTrue()
    {
        alive = true;
    }

    public void OnDeath()
    {
        if (UIScript.lives > 0)
        {
            ShipAnimator.Play("ShipRespawn");
        }
        else
        {
            alive = false;
            UIScript.GameOver();
        }
    }

    public void PlayerDeath()
    {
        ShipRigibody.velocity = Vector3.zero;
        alive = false;
        ShipAnimator.Play("ShipDeath");
    }
}
