using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    public float ProjectileSpeed;
    public GameObject Enemies;
    public UIManagerScript UIManager;
    public EnemieMovementScript EnemieScript;

    void Start()
    {
        Enemies = GameObject.FindWithTag("AllEnemies");
        UIManager = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
        EnemieScript = Enemies.GetComponent<EnemieMovementScript>();
    }

    void Update()
    {
        transform.Translate(Vector2.up * ProjectileSpeed * Time.deltaTime);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            UIManager.EnemiesMinusOne();
            UIManager.UpdateScore(50);
            EnemieScript.AddSpeed();
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Enemyx")
        {
            UIManager.EnemiesMinusOne();
            UIManager.UpdateScore(100);
            EnemieScript.AddSpeed();
            Destroy(gameObject);
        }
    }

}
