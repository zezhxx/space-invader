using UnityEngine;

public class BoundaryScript : MonoBehaviour
{
    public GameObject AllEnemies;
    public EnemieMovementScript EnemieScript;

    void Start()
    {
        SearchEnemies();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Enemyx")
        {
            EnemieScript.MovingDown();
        }
    }

    public void SearchEnemies()
    {
        AllEnemies = GameObject.FindWithTag("AllEnemies");
        EnemieScript = AllEnemies.GetComponent<EnemieMovementScript>();
    }
}
