using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManagerScript : MonoBehaviour
{
    public GameObject GameOverScreen, Enemies;
    public Animator TransitionAnimator;
    public int lives, enemylives, score, round;
    public Image[] LivesUI;
    public TMP_Text ScoreText, FinalScoreText, HighScoreText;

    // Start is called before the first frame update
    void Start()
    {
        Enemies = GameObject.FindWithTag("AllEnemies");
        lives = 3;
        ScoreText.text = "Score: " + score;
        enemylives = 21;
        round = 1;
    }

    void Update()
    {
        if (enemylives <= 0)
        {
            ResetScene();
        }
    }

    public void LivesMinusOne()
    {
        lives--;
        for (int i = 0; i < LivesUI.Length; i++)
        {
            if (i < lives)
            {
                LivesUI[i].enabled = true;
            }
            else
            {
                LivesUI[i].enabled = false;
            }
        }
    }
    
    public void LivesMinusAll()
    {
        lives -= 3;
        LivesUI[0].enabled = false;
        LivesUI[1].enabled = false;
        LivesUI[2].enabled = false;
    }

    public void UpdateScore(int points)
    {
        score += points;
        ScoreText.text = "Score: " + score;
    }

    public void EnemiesMinusOne()
    {
        enemylives--;
        if (enemylives <= 0)
        {
            round++;
            Destroy(Enemies);
            ResetScene();
        }
    }

    public void ResetScene()
    {
        TransitionAnimator.Play("NewEnemies");
        enemylives = 21;
        
    }

    public void SearchEnemies()
    {
        Enemies = GameObject.FindWithTag("AllEnemies");
    }

    public void GameOver()
    {
        HighScoreUpdate();
        GameOverScreen.SetActive(true);
        Cursor.visible = true;
    }

    public void HighScoreUpdate()
    {
        if (PlayerPrefs.HasKey("SavedHighScore"))
        {
            if (score > PlayerPrefs.GetInt("SavedHighScore"))
            {
                PlayerPrefs.SetInt("SavedHighScore", score);
            }
        }
        else
        {
            PlayerPrefs.SetInt("SavedHighScore", score);
        }

        FinalScoreText.text = "Score: " + score;
        HighScoreText.text = "HighScore: " + PlayerPrefs.GetInt("SavedHighScore");

    }
}
