using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public GameObject PausePanel, Ship, Enemies;
    public bool IsPaused;
    public BoundaryScript Left, Right;
    public UIManagerScript UI;
    private Animator Transitions;
    private ShipScript shipScript;

    void Start()
    {
        Transitions = GetComponent<Animator>();
        shipScript = Ship.GetComponent<ShipScript>();
    }

    public void PauseAction()
    {
        if (IsPaused)
            ResumeGame();
        else
            PauseGame();
    }
    public void PauseGame()
    {
        Cursor.visible = true;
        Time.timeScale = 0;
        PausePanel.SetActive(true);
        IsPaused = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        PausePanel.SetActive(false);
        IsPaused = false;
    }

    public void AliveToTrue()
    {
        shipScript.alive = true;
    }

    public void AliveToFalse()
    {
        shipScript.alive = false;
    }

    public void RespawnEnemies()
    {
        Instantiate(Enemies);
    }

    public void EnemiesSearch()
    {
        Left.SearchEnemies();
        Right.SearchEnemies();
        UI.SearchEnemies();
    }

    public void ExitGame()
    {
        Time.timeScale = 1;
        Transitions.Play("ExitTransition");
    }

    public void ExitAction()
    {
        Application.Quit();
    }

    public void ResetGame()
    {
        Transitions.Play("ResetTransition");
    }

    private void ResetAction()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void TransitionToGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
