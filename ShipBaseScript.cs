using UnityEngine;

public class ShipBaseScript : MonoBehaviour
{
    public GameObject Ship, UIManager;
    private UIManagerScript UIScript;
    private ShipScript ShipScrpt;

    void Start()
    {
        UIScript = UIManager.GetComponent<UIManagerScript>();
        ShipScrpt = Ship.GetComponent<ShipScript>();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Enemyx")
        {
            UIScript.LivesMinusAll();
            ShipScrpt.PlayerDeath();
        }
    }
}
