using UnityEngine;

public class EnemyProjectileScript : MonoBehaviour
{
    public float ProjectileSpeed;
    private ShipScript ShipScript;

    void Start()
    {
        ShipScript = GameObject.Find("Ship").GetComponent<ShipScript>();
        ProjectileSpeed = 4;
    }

    void Update()
    {
        transform.Translate(Vector2.up * ProjectileSpeed * Time.deltaTime);

        if (ShipScript.alive == false)
        {
            Destroy(gameObject);
        }
    }
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
