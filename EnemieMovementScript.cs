using UnityEngine;

public class EnemieMovementScript : MonoBehaviour
{
    public float MovementSpeed, timer, SpeedAdded;
    private ShipScript ScriptFromShip;
    private UIManagerScript UI;

    void Start()
    {
        ScriptFromShip = GameObject.Find("Ship").GetComponent<ShipScript>();
        UI = GameObject.Find("UIManager").GetComponent<UIManagerScript>();
        MovementSpeed = 0.5f;
        SpeedAdded = 0.03f + UI.round * 0.02f;
    }

    void Update()
    {
        if (ScriptFromShip.alive == true)
        {
            transform.Translate(Vector2.right * MovementSpeed * Time.deltaTime);
        }

        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }
    }

    public void MovingDown()
    {
        if (timer <= 0)
        {
            timer += 0.5f;
            MovementSpeed *= -1;
            transform.position = new Vector3(transform.position.x, transform.position.y - 1, transform.position.z);
        }
    }

    public void AddSpeed()
    {
        if (MovementSpeed < 0)
        {
            MovementSpeed -= SpeedAdded;
        }
        else
        {
            MovementSpeed += SpeedAdded;
        }
    }
}
