using UnityEngine;

public class AlienScript : MonoBehaviour
{
    public AudioClip Explosion;
    public GameObject EnemyProjectile;
    public float SpawnTimer;
    private Animator AlienAnimator;
    private AudioSource Audio;
    void Start()
    {
        AlienAnimator = GetComponent<Animator>();
        SpawnTimer = Random.Range(5, 25);
        Audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        SpawnTimer -= Time.deltaTime;
        if (SpawnTimer <= 0)
        {
            Instantiate(EnemyProjectile, new Vector3(transform.position.x, transform.position.y - 0.6f, transform.position.z), Quaternion.Euler(new Vector3(0, 0, 180)));
            SpawnTimer = Random.Range(7, 25) + 0.25f;
        }
    }
    public void OnDeath()
    {
        Destroy(this.gameObject);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Projectile")
        {
            Audio.PlayOneShot(Explosion, 0.5f);
            AlienAnimator.Play("AlienDeath");
        }
    }
}
